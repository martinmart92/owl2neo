# OWL2NEO

## Description : 

A demonstration example of a OWL-to-Neo4j converter.

0-Clear Neo4j repository
1. Reads an OWL file
2. Transform it into graph-modeled data
3. Writes it into a Neo4j repository
4-reports a summary of count of nodes and relationships created in neo4j

##compile 

```
mvn clean install
```

##run : 
```
java -Dowl.input.file=/home/martin/IdeaProjects/owl2neo/src/main/resources/owl/pizza.owl\
 -Dneo.repository.url=bolt://localhost\
 -Dneo.repository.login=neo4j\
 -Dneo.repository.password=martin4j\
 -Dneo.clean.on.start=true\
 -jar target/owl2neo-1.0-SNAPSHOT.jar
```
##input
an OWL file, such as the pizza ontology
Currently, as it is a demonstration project, only a sub-part of OWL-API data structures are covered.

##output

sends Cypher write-queries into a Neo4j repository

##Examples of query after conversion


Give me all the restriction allValuesFrom, that are subClassOf the FruttiDiMare OwlClass
```
MATCH (cl:OwlClass)-[r:SubClassOf]->(restr:Restriction)
WHERE cl.uri = 'http://www.co-ode.org/ontologies/pizza/pizza.owl#FruttiDiMare'
	AND
	restr.`http://www.w3.org/2002/07/owl#allValuesFrom`
RETURN restr LIMIT 25
```


Give me all the classes disjoint from TomatoTopping
```
MATCH p=(cl1:OwlClass)-[r:DisjointWith]->(cl2:OwlClass)
WHERE cl1.uri = 'http://www.co-ode.org/ontologies/pizza/pizza.owl#TomatoTopping'
RETURN cl2 LIMIT 25
```



Give me all the non-anonymous(i.e.:OwlClass) super-classes of LeekTopping
```
MATCH p=(cl1:OwlClass)-[r:SubClassOf]->(cl2:OwlClass)
WHERE cl1.uri = 'http://www.co-ode.org/ontologies/pizza/pizza.owl#LeekTopping'
RETURN cl2 LIMIT 25
```

##future ideas

As the module transform the input data into an internal graph data structure before writing the output,
implementing other reading/or writing modules could be easily done: such as replacing owlapi with rdf4j
Output graph modeling can also be changed.

