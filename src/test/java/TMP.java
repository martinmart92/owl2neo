import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by martin on 21/08/16.
 */
public class TMP {


    public static void main(String[] args) {

        System.out.println("TMP");
        Map<String, Object> payload = new HashMap<String, Object>();
        payload.put("v1Key", "v1Val");
        payload.put("http://www.w3.org/2002/07/owl#versionInfo", "vinfoVal");
        String json = new Gson().toJson( payload );
        System.out.println("json:\n"+json);

    }
}
