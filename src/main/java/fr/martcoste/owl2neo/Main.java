package fr.martcoste.owl2neo;

import com.google.inject.Guice;
import com.google.inject.Injector;
import fr.martcoste.owl2neo.conf.Module;
import fr.martcoste.owl2neo.engine.EngineIfc;
import fr.martcoste.owl2neo.read.ReaderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by martin on 20/08/16.
 */
public class Main {

    protected static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {

        LOG.info("starting owl2neo app");
        try {

            Injector injector = Guice.createInjector(new Module());
            EngineIfc engine = injector.getInstance(EngineIfc.class);

            engine.init();

            engine.process();

            engine.finish();
        } catch (Exception e) {
            LOG.error("an error occurred", e);
            System.exit(-1);
        }
        LOG.info("finished owl2neo app");
    }
}
