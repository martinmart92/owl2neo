package fr.martcoste.owl2neo.engine;

import fr.martcoste.owl2neo.consumer.ConsumerException;
import fr.martcoste.owl2neo.read.ReaderException;

/**
 * Created by martin on 20/08/16.
 */
public interface EngineIfc {


    void init() throws ReaderException, ConsumerException;

    void process() throws Exception;

    void finish();
}
