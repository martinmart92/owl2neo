package fr.martcoste.owl2neo.engine;

import com.google.inject.Inject;
import fr.martcoste.owl2neo.consumer.ConsumerException;
import fr.martcoste.owl2neo.consumer.ConsumerIfc;
import fr.martcoste.owl2neo.model.graph.Graph;
import fr.martcoste.owl2neo.model.owl.OntologyIfc;
import fr.martcoste.owl2neo.read.OwlReaderIfc;
import fr.martcoste.owl2neo.read.ReaderException;
import fr.martcoste.owl2neo.transform.owlapi.OwlApiTransformersPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by martin on 20/08/16.
 */
public class Engine implements EngineIfc {


    private final Set<ConsumerIfc> consumers;
    private final OwlReaderIfc owlReader;

    protected static final Logger LOG = LoggerFactory.getLogger(Engine.class);
    private OntologyIfc ontology;
    private Graph graph;

    @Inject
    public Engine(@Nonnull Set<ConsumerIfc> consumers,
                  @Nonnull OwlReaderIfc owlReader) {
        this.consumers = consumers;
        this.owlReader = owlReader;
    }
    @Override
    public void init() throws ReaderException, ConsumerException {
        LOG.debug("init enter");
        List<ConsumerException> cs = new ArrayList<>();
        this.consumers.forEach((consumerIfc) -> {
            if (!cs.isEmpty()) return;

            try {
                consumerIfc.init();
            } catch (ConsumerException e) {
                cs.add(e);
            }
        });
        if (!cs.isEmpty()) {
            LOG.error("error while init consumers!");
            throw cs.get(0);
        }
        LOG.debug(String.format("initiated %d consumers", this.consumers.size()));

        LOG.debug("load read input");
        this.ontology = this.owlReader.readFile();

        LOG.debug("init done");
    }

    @Override
    public void process() throws Exception {
        LOG.debug("process");

        LOG.debug("transform read input into graph");
        transform();

        LOG.debug("consume read input");
        consume();
    }

    private void transform() {

        //execute a pipeline of transformers
        OwlApiTransformersPipeline.build().forEach(t -> {
            if (t.canTransform(this.ontology)) {
                this.graph = t.transform(this.ontology, this.graph);
            }
        });
    }


    private void consume() throws Exception {
        List<Exception> errors = new ArrayList<>();
        //meta
        this.consumers.
                forEach(c->{
                    if (errors.isEmpty()) {

                        try {
                            c.consume(this.graph.getMetadata());
                        } catch (ConsumerException e) {
                            errors.add(e);
                        }
                    }
                });
        checkErrors(errors);
        //nodes
        this.consumers.forEach(
                c->{
                    this.graph.getNodesParallelStream()
                            .forEach(n -> {
                                if (errors.isEmpty()) {
                                    try {
                                        c.consume(n);
                                    } catch (ConsumerException e) {
                                        errors.add(e);
                                    }
                                }
                            });

                });
        checkErrors(errors);

        //relations
        this.consumers.forEach(
                c->{
                    this.graph.getRelations().parallelStream()
                            .forEach(r -> {
                                if (errors.isEmpty()) {
                                    try {
                                        c.consume(r);
                                    } catch (ConsumerException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                });
        checkErrors(errors);

    }

    private void checkErrors(List<Exception> errors) throws Exception {

        if (errors==null || errors.isEmpty()) return;
        throw errors.get(0);
    }


    @Override
    public void finish() {
        LOG.debug("finish engine work");
        this.consumers.forEach(ConsumerIfc::finish);
    }
}
