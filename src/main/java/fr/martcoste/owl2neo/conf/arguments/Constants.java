package fr.martcoste.owl2neo.conf.arguments;

/**
 * Created by martin on 20/08/16.
 */
public class Constants {


    /**
     * absolute file path of the owl input file
     */
    public final static String OWL_INPUT_FILE = "owl.input.file";
    /**
     *   Neo4j repository url. Example: "bolt://localhost"
     */
    public final static String NEO_REPOSITORY_URL = "neo.repository.url";
    /**
     * Neo4j username. Example: "neo4j"
     */
    public final static String NEO_REPOSITORY_LOGIN = "neo.repository.login";
    /**
     * Neo4j password. Example: "neo4j"
     */
    public final static String NEO_REPOSITORY_PASSWORD = "neo.repository.password";
    /**
     *   Neo4j repository url. Example: "bolt://localhost"
     */
    public final static String NEO_CLEAN_ON_START = "neo.clean.on.start";

}
