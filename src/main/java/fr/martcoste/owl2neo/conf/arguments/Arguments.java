package fr.martcoste.owl2neo.conf.arguments;

import java.util.Map;

/**
 * Created by martin on 20/08/16.
 */
public interface Arguments {

    Map<String, String> getArguments();
}
