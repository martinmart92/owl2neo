package fr.martcoste.owl2neo.conf.arguments;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by martin on 20/08/16.
 */
public class JavaPropertiesArguments implements Arguments {

    protected static final Logger LOG = LoggerFactory.getLogger(JavaPropertiesArguments.class);

    private static Map<String, String> arguments;

    @Override
    public Map<String, String> getArguments() {

        if (arguments!=null) {
            return arguments;
        }
        LOG.info("init read argument map...");

        arguments = new HashMap<>();

        Set<String> keys = new HashSet<>();
        keys.add(Constants.OWL_INPUT_FILE);
        keys.add(Constants.NEO_REPOSITORY_URL);
        keys.add(Constants.NEO_REPOSITORY_LOGIN);
        keys.add(Constants.NEO_REPOSITORY_PASSWORD);
        keys.add(Constants.NEO_CLEAN_ON_START);

        keys.forEach(k->{
            arguments.put(k, System.getProperty(k));
        });

        LOG.info("read argument map:");
        for (Map.Entry<String, String> entry : arguments.entrySet()) {
            LOG.debug("key:'"+entry.getKey()+"'/value:'"+entry.getValue()+"'");
        }
        return arguments;
    }
}
