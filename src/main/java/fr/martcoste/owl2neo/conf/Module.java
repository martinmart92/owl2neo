package fr.martcoste.owl2neo.conf;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import fr.martcoste.owl2neo.conf.arguments.Arguments;
import fr.martcoste.owl2neo.conf.arguments.JavaPropertiesArguments;
import fr.martcoste.owl2neo.consumer.ConsumerIfc;
import fr.martcoste.owl2neo.consumer.CountConsumer;
import fr.martcoste.owl2neo.consumer.Neo4JConsumer;
import fr.martcoste.owl2neo.engine.Engine;
import fr.martcoste.owl2neo.engine.EngineIfc;
import fr.martcoste.owl2neo.read.owlapi.OWLAPIReader;
import fr.martcoste.owl2neo.read.OwlReaderIfc;

/**
 * Created by martin on 20/08/16.
 */
public class Module extends AbstractModule {
    @Override
    protected void configure() {

        //arguments
        bind(Arguments.class).to(JavaPropertiesArguments.class);

        //consumers
        Multibinder<ConsumerIfc> consumerBinder = Multibinder.newSetBinder(binder(), ConsumerIfc.class);
        consumerBinder.addBinding().to(Neo4JConsumer.class);
        consumerBinder.addBinding().to(CountConsumer.class);

        //read reader
        bind(OwlReaderIfc.class).to(OWLAPIReader.class);

        //engine
        bind(EngineIfc.class).to(Engine.class);

    }
}
