package fr.martcoste.owl2neo.transform;

import fr.martcoste.owl2neo.model.graph.Graph;
import fr.martcoste.owl2neo.model.owl.OntologyIfc;

import javax.annotation.Nonnull;

/**
 * Created by martin on 20/08/16.
 */
public interface TransformerIfc {

    boolean canTransform(@Nonnull OntologyIfc input);

    @Nonnull
    Graph transform(@Nonnull OntologyIfc input, @Nonnull Graph graph);

}
