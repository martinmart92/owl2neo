package fr.martcoste.owl2neo.transform.owlapi;

import fr.martcoste.owl2neo.model.graph.Graph;
import fr.martcoste.owl2neo.model.graph.GraphMetadata;
import fr.martcoste.owl2neo.model.graph.Property;
import fr.martcoste.owl2neo.model.owl.OntologyIfc;
import fr.martcoste.owl2neo.model.owl.owlapi.OWLApiOntology;
import fr.martcoste.owl2neo.transform.TransformerIfc;
import org.openrdf.model.vocabulary.OWL;
import org.openrdf.model.vocabulary.RDFS;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.util.OWLOntologyWalkerVisitorEx;

import javax.annotation.Nonnull;

/**
 *
 *
 * Created by martin on 20/08/16.
 */
public class GraphCreatorTransformer extends OWLApiTransformer {


    @Nonnull
    @Override
    public Graph transform(@Nonnull OWLOntology ontology, @Nonnull Graph graph) {
        Graph g = new Graph();
        GraphMetadata meta = new GraphMetadata();
        g.setMetadata(meta);

        //basic onto properties
        ontology.getAnnotations()
                .forEach(a-> {
                    if (a.getProperty().getIRI().toString().equals(RDFS.COMMENT.toString())
                            ||
                            a.getProperty().getIRI().toString().equals(OWL.VERSIONINFO.toString())
                            ) {
                        meta.getProperties().add(toProperty(a));
                    }
                });
        return g;
    }

    protected Property toProperty(OWLAnnotation annotation) {
        Property p = new Property();
        p.setKey(annotation.getProperty().getIRI().toString());
        if (annotation.getValue().asLiteral().isPresent()) {
            OWLLiteral value = annotation.getValue().asLiteral().get();
            p.setLanguage(value.getLang());
            p.setValue(value.getLiteral());
            p.setDatatype(value.getDatatype().toString());
        }
        return p;
    }
}
