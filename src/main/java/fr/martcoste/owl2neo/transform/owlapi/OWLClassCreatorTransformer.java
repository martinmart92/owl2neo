package fr.martcoste.owl2neo.transform.owlapi;

import fr.martcoste.owl2neo.model.graph.Graph;
import fr.martcoste.owl2neo.model.graph.Node;
import fr.martcoste.owl2neo.owl.OwlConstants;
import fr.martcoste.owl2neo.transform.owlapi.sub.OWLClassLabelExtractor;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by martin on 21/08/16.
 */
public class OWLClassCreatorTransformer extends OWLApiTransformer {


    @Override
    protected Graph transform(@Nonnull OWLOntology ontology, @Nonnull Graph graph) {
        // the named classes referenced by axioms in the ontology.
        List<Node> nodes = new ArrayList<>();
        for (OWLClass cls : ontology.getClassesInSignature()) {
            Node n = new Node();
            n.setLabel(OwlConstants.LABEL_OWL_CLASS);
            n.setId(UUID.randomUUID().toString());
            if (cls.getIRI()!=null) {
                n.setUri(cls.getIRI().toURI().toASCIIString());
            }
            OWLClassLabelExtractor extractor = new OWLClassLabelExtractor();
            n.getProperties().addAll(extractor.extractLabel(ontology, cls.getIRI()));
            nodes.add(n);
        }

        graph.addAllNodes(nodes);
        return graph;
    }
}
