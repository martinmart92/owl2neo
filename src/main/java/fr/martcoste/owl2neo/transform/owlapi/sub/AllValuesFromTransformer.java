package fr.martcoste.owl2neo.transform.owlapi.sub;

import fr.martcoste.owl2neo.model.graph.Graph;
import fr.martcoste.owl2neo.model.graph.Node;
import fr.martcoste.owl2neo.model.graph.Property;
import fr.martcoste.owl2neo.model.graph.Relation;
import fr.martcoste.owl2neo.owl.OwlConstants;
import fr.martcoste.owl2neo.utils.GraphUtils;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Set;
import java.util.UUID;

/**
 * Created by martin on 23/08/16.
 */
public class AllValuesFromTransformer {

    protected static final Logger LOG = LoggerFactory.getLogger(AllValuesFromTransformer.class);

    /**
     * transform allValuesFrom attached to a class into graph objects
     *
     *input example:
     <owl:Class rdf:about="#FruttiDiMare">
         <rdfs:subClassOf>
             <owl:Restriction>
             <owl:onProperty rdf:resource="#hasTopping"/>
             <owl:allValuesFrom>
                 <owl:Class>
                     <owl:unionOf rdf:parseType="Collection">
                         <rdf:Description rdf:about="#MixedSeafoodTopping"/>
                         <rdf:Description rdf:about="#TomatoTopping"/>
                         <rdf:Description rdf:about="#GarlicTopping"/>
                     </owl:unionOf>
                 </owl:Class>
             </owl:allValuesFrom>
             </owl:Restriction>
         </rdfs:subClassOf>
     </owl:Class>

     output example:
     node : label=restriction
     id=uuid-generated
     label="Restriction",
     uri=null,
     onproperty=the property #hasTopping in the allValuesFrom
     allValuesFrom=the list of target class #Mild in the allValuesFrom (implicitely in union)

     a relationship: label "subClassOf"
     id=uuid-generated value
     from=#FruttiDiMare, uri of the input class
     to=the restriction node

     * @param allValuesFrom
     * @param clazz
     * @param graph
     * @return
     */
    @Nonnull
    public Graph transform(@Nonnull Set<OWLObjectAllValuesFrom> allValuesFrom, @Nonnull OWLClass clazz, @Nonnull Graph graph) {

        for (OWLObjectAllValuesFrom avf : allValuesFrom) {
            Node restriction = new Node();
            restriction.setId(UUID.randomUUID().toString());
            restriction.setUri(null);
            restriction.setLabel(OwlConstants.LABEL_RESTRICTION);

            Property onPropertyP = new Property();
            onPropertyP.setKey(OwlConstants.PROPERTY_KEY_ON_PROPERTY);
            onPropertyP.setValue(avf.getProperty().getNamedProperty().getIRI().toURI().toASCIIString());
            restriction.getProperties().add(onPropertyP);

            for (OWLClassExpression inUnion : avf.getFiller().asDisjunctSet()) {
                Property allValuesFromP = new Property();
                allValuesFromP.setKey(OwlConstants.PROPERTY_KEY_ALL_VALUES_FROM);
                allValuesFromP.setValue(inUnion.asOWLClass().getIRI().toURI().toASCIIString());
                restriction.getProperties().add(allValuesFromP);
            }

            Relation relationSubClassOf = new Relation();
            relationSubClassOf.setLabel(OwlConstants.LABEL_SUBCLASS_OF);
            relationSubClassOf.setId(UUID.randomUUID().toString());
            relationSubClassOf.setFrom(GraphUtils.findAlreadyCreatedClass(clazz.getIRI(), graph));
            relationSubClassOf.setTo(restriction);

            graph.addNode(restriction);
            graph.getRelations().add(relationSubClassOf);
        }
        return graph;
    }
}
