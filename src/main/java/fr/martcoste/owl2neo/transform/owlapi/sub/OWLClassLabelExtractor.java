package fr.martcoste.owl2neo.transform.owlapi.sub;

import fr.martcoste.owl2neo.model.graph.Property;
import org.semanticweb.owlapi.model.*;

import javax.annotation.Nonnull;

import java.util.ArrayList;
import java.util.List;

import static org.semanticweb.owlapi.search.Searcher.annotationObjects;

/**
 * Created by martin on 21/08/16.
 */
public class OWLClassLabelExtractor {


    public List<Property> extractLabel(@Nonnull OWLOntology ontology, @Nonnull IRI classIri) {

        List<Property> properties = new ArrayList<>();
        OWLDataFactory df = ontology.getOWLOntologyManager().getOWLDataFactory();
        for (OWLAnnotation annotation : annotationObjects(ontology.getAnnotationAssertionAxioms(classIri), df.getRDFSLabel())) {
            if (annotation.getValue() instanceof OWLLiteral) {
                Property p = new Property();
                if (annotation.getProperty().getIRI()!=null) {
                    p.setKey(annotation.getProperty().getIRI().toURI().toASCIIString());
                }
                OWLLiteral val = (OWLLiteral) annotation.getValue();
                p.setLanguage(val.getLang());
                p.setDatatype(val.getDatatype().toString());
                p.setValue(val.getLiteral());
                properties.add(p);
            }
        }

        return properties;
    }
}
