package fr.martcoste.owl2neo.transform.owlapi;

import fr.martcoste.owl2neo.model.graph.Graph;
import fr.martcoste.owl2neo.model.graph.Relation;
import fr.martcoste.owl2neo.owl.OwlConstants;
import fr.martcoste.owl2neo.utils.GraphUtils;
import org.semanticweb.owlapi.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Created by martin on 23/08/16.
 */
public class OWLClassDisjointsTransformer extends OWLApiTransformer{

    protected static final Logger LOG = LoggerFactory.getLogger(OWLClassDisjointsTransformer.class);


    @Override
    protected Graph transform(@Nonnull OWLOntology ontology, @Nonnull Graph graph) {
        for (OWLClass cls : ontology.getClassesInSignature()) {
            graph.getRelations().addAll(
                    extractDisjoints(ontology, cls, graph));
        }
        return graph;
    }

    /**
     * transform disjointWith attached to a class into graph objects
     *
     *input example:
     <owl:Class rdf:about="#TomatoTopping">
         <owl:disjointWith rdf:resource="#LeekTopping"/>
         <owl:disjointWith rdf:resource="#MushroomTopping"/>
         <owl:disjointWith rdf:resource="#ArtichokeTopping"/>
         <owl:disjointWith rdf:resource="#OnionTopping"/>
         <owl:disjointWith rdf:resource="#SpinachTopping"/>
         <owl:disjointWith rdf:resource="#PetitPoisTopping"/>
         <owl:disjointWith rdf:resource="#AsparagusTopping"/>
     </owl:Class>

     output example:
     a list of relationships: label "DisjointWith"
     id=uuid-generated value
     from=#TomatoTopping, uri of the source class
     to=#LeekTopping, uri of the target class

     * @param ontology
     * @param clazz
     * @return graph
     */
    @Nonnull
    public Collection<Relation> extractDisjoints(@Nonnull OWLOntology ontology,
                                                 @Nonnull OWLClass clazz,
                                                 @Nonnull Graph graph) {
        List<Relation> rels = new ArrayList<>();
        for (OWLDisjointClassesAxiom disjointsAxioms : ontology.getDisjointClassesAxioms(clazz)) {
            for (OWLClassExpression cl : disjointsAxioms.getClassExpressions()) {
                IRI iri = cl.asOWLClass().getIRI();
                if (!iri.equals(clazz.getIRI())) {
                    Relation rel = new Relation();
                    rel.setId(UUID.randomUUID().toString());
                    rel.setLabel(OwlConstants.LABEL_DISJOINT_WITH);
                    rel.setFrom(GraphUtils.findAlreadyCreatedClass(clazz.getIRI(), graph));
                    rel.setTo(GraphUtils.findAlreadyCreatedClass(iri, graph));
                    rels.add(rel);
                }
            }
        }
        return rels;
    }
}
