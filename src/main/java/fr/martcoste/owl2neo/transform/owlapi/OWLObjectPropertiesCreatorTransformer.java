package fr.martcoste.owl2neo.transform.owlapi;

import fr.martcoste.owl2neo.model.graph.Graph;
import fr.martcoste.owl2neo.transform.owlapi.sub.AllValuesFromTransformer;
import fr.martcoste.owl2neo.transform.owlapi.sub.SomeValuesFromTransformer;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.util.OWLClassExpressionVisitorAdapter;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by martin on 21/08/16.
 */
public class OWLObjectPropertiesCreatorTransformer extends OWLApiTransformer {

    @Override
    protected Graph transform(@Nonnull OWLOntology ontology, @Nonnull Graph graph) {

        //extract OWLClasses restrictions (someValuesFrom, allValuesFrom, etc.)

        for (OWLClass c : ontology.getClassesInSignature()) {

            //from : https://github.com/owlcs/owlapi/blob/version4/contract/src/test/java/uk/ac/manchester/owl/owlapi/tutorialowled2011/TutorialSnippetsTestCase.java, method : testLookupRestrictions
            RestrictionVisitor visitor = new RestrictionVisitor(Collections.singleton(ontology));
            for (OWLSubClassOfAxiom ax : ontology.getSubClassAxiomsForSubClass(c)) {
                ax.getSuperClass().accept(visitor);
            }
            SomeValuesFromTransformer someValuesFromTransformer = new SomeValuesFromTransformer();
            graph = someValuesFromTransformer.transform(visitor.getSomeValuesFrom(), c, graph);

            AllValuesFromTransformer allValuesFromTransformer = new AllValuesFromTransformer();
            graph = allValuesFromTransformer.transform(visitor.getAllValuesFrom(), c, graph);
        }
        return graph;
    }


    /**
     * From : https://github.com/owlcs/owlapi/blob/version4/contract/src/test/java/uk/ac/manchester/owl/owlapi/tutorialowled2011/TutorialSnippetsTestCase.java
     * Visits existential restrictions and collects the properties which are
     * restricted
     */
    private static class RestrictionVisitor extends OWLClassExpressionVisitorAdapter {

        @Nonnull
        private final Set<OWLClass> processedClasses;
        @Nonnull
        private final Set<OWLObjectSomeValuesFrom> someValuesFrom;
        @Nonnull
        private final Set<OWLObjectAllValuesFrom> allValuesFrom;
        private final Set<OWLOntology> onts;

        RestrictionVisitor(Set<OWLOntology> onts) {
            someValuesFrom = new HashSet<>();
            allValuesFrom = new HashSet<>();
            processedClasses = new HashSet<>();
            this.onts = onts;
        }

        @Nonnull
        public Set<OWLObjectSomeValuesFrom> getSomeValuesFrom() {
            return someValuesFrom;
        }

        @Nonnull
        public Set<OWLObjectAllValuesFrom> getAllValuesFrom() {
            return allValuesFrom;
        }

        @Override
        public void visit(OWLClass ce) {
            if (!processedClasses.contains(ce)) {
                processedClasses.add(ce);
                for (OWLOntology ont : onts) {
                    for (OWLSubClassOfAxiom ax : ont.getSubClassAxiomsForSubClass(ce)) {
                        ax.getSuperClass().accept(this);
                    }
                }
            }
        }

        @Override
        public void visit(@Nonnull OWLObjectSomeValuesFrom ce) {
            someValuesFrom.add(ce);
        }
        @Override
        public void visit(@Nonnull OWLObjectAllValuesFrom avf) {
            allValuesFrom.add(avf);
        }
    }
}
