package fr.martcoste.owl2neo.transform.owlapi;

import fr.martcoste.owl2neo.model.graph.Graph;
import fr.martcoste.owl2neo.model.owl.OntologyIfc;
import fr.martcoste.owl2neo.model.owl.owlapi.OWLApiOntology;
import fr.martcoste.owl2neo.transform.TransformerIfc;
import org.semanticweb.owlapi.model.OWLOntology;

import javax.annotation.Nonnull;

/**
 * Created by martin on 21/08/16.
 */
public abstract class OWLApiTransformer implements TransformerIfc {

    /**
     * owl-api object
     */
    private OWLOntology ontology;

    @Override
    public boolean canTransform(@Nonnull OntologyIfc input) {
        return (input instanceof OWLApiOntology);
    }


    @Nonnull
    @Override
    public Graph transform(@Nonnull OntologyIfc input, @Nonnull Graph graph) {
        ontology = ((OWLApiOntology) input).getOntology();
        return transform(ontology, graph);
    }

    protected abstract Graph transform(@Nonnull OWLOntology ontology, @Nonnull Graph graph);
}
