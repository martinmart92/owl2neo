package fr.martcoste.owl2neo.transform.owlapi;

import fr.martcoste.owl2neo.transform.TransformerIfc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martin on 23/08/16.
 */
public class OwlApiTransformersPipeline {
    /**
     * define a list of transformers
     * @return
     */
    public static List<TransformerIfc> build() {
        List<TransformerIfc> transformers = new ArrayList<>();
        //N.B.:the order of transformers is important, as nodes must be created before relationships
        transformers.add(new GraphCreatorTransformer());
        transformers.add(new OWLClassCreatorTransformer());
        transformers.add(new OWLClassDisjointsTransformer());
        transformers.add(new OWLClassSubClassTransformer());
        transformers.add(new OWLObjectPropertiesCreatorTransformer());
        return transformers;
    }
}
