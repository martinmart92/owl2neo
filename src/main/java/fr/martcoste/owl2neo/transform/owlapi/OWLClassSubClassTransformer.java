package fr.martcoste.owl2neo.transform.owlapi;

import fr.martcoste.owl2neo.model.graph.Graph;
import fr.martcoste.owl2neo.model.graph.Relation;
import fr.martcoste.owl2neo.owl.OwlConstants;
import fr.martcoste.owl2neo.utils.GraphUtils;
import org.semanticweb.owlapi.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Created by martin on 23/08/16.
 */
public class OWLClassSubClassTransformer extends OWLApiTransformer{

    protected static final Logger LOG = LoggerFactory.getLogger(OWLClassSubClassTransformer.class);


    @Override
    protected Graph transform(@Nonnull OWLOntology ontology, @Nonnull Graph graph) {
        for (OWLClass cls : ontology.getClassesInSignature()) {
            graph.getRelations().addAll(
                    extractClassSubClasses(ontology, cls, graph));
        }
        return graph;
    }

    /**
     * transform subClass attached to a class into graph objects
     *
     *input example:
     <owl:Class rdf:about="#TomatoTopping">
        <rdfs:subClassOf rdf:resource="#VegetableTopping"/>
     </owl:Class>

     output example:
     a list of relationships: label "SubClassOf"
     id=uuid-generated value
     from=#TomatoTopping, uri of the source class
     to=#VegetableTopping, uri of the target class

     * @param ontology
     * @param clazz
     * @return graph
     */
    @Nonnull
    public Collection<Relation> extractClassSubClasses(@Nonnull OWLOntology ontology,
                                                       @Nonnull OWLClass clazz,
                                                       @Nonnull Graph graph) {
        List<Relation> rels = new ArrayList<>();
        for (OWLSubClassOfAxiom subClass : ontology.getSubClassAxiomsForSubClass(clazz)) {

            OWLClassExpression superClass = subClass.getSuperClass();
            if (superClass.isAnonymous()) {
                continue;
            }
            OWLClass owlClassSuper = superClass.asOWLClass();
            Relation rel = new Relation();
            rel.setId(UUID.randomUUID().toString());
            rel.setLabel(OwlConstants.LABEL_SUBCLASS_OF);
            rel.setFrom(GraphUtils.findAlreadyCreatedClass(clazz.getIRI(), graph));
            rel.setTo(GraphUtils.findAlreadyCreatedClass(owlClassSuper.getIRI(), graph));
            rels.add(rel);
        }
        return rels;
    }
}
