package fr.martcoste.owl2neo.transform.owlapi.sub;

import fr.martcoste.owl2neo.model.graph.Graph;
import fr.martcoste.owl2neo.model.graph.Node;
import fr.martcoste.owl2neo.model.graph.Property;
import fr.martcoste.owl2neo.model.graph.Relation;
import fr.martcoste.owl2neo.owl.OwlConstants;
import fr.martcoste.owl2neo.utils.GraphUtils;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;

import javax.annotation.Nonnull;
import java.util.Set;
import java.util.UUID;

/**
 * Created by martin on 23/08/16.
 */
public class SomeValuesFromTransformer {

    /**
     * transform some values from attached to a class into graph objects
     *
     *input example:
     <owl:Class rdf:about="#LeekTopping">
         <rdfs:subClassOf>
             <owl:Restriction>
                 <owl:onProperty rdf:resource="#hasSpiciness"/>
                 <owl:someValuesFrom rdf:resource="#Mild"/>
             </owl:Restriction>
         </rdfs:subClassOf>
     </owl:Class>

     output example:
     node : label=restriction
            id=uuid-generated
            label="Restriction",
            uri=null,
            onproperty=the property #hasSpiciness in the someValuesFrom
            someValuesFrom=the target class #Mild in the someValuesFrom

     a relationship: label "subClassOf"
            id=uuid-generated value
            from=#LeekTopping, uri of the input class
            to=the restriction node

     * @param someValuesFrom
     * @param clazz
     * @param graph
     * @return
     */
    @Nonnull
    public Graph transform(@Nonnull Set<OWLObjectSomeValuesFrom> someValuesFrom, OWLClass clazz, @Nonnull Graph graph) {

        for (OWLObjectSomeValuesFrom svf : someValuesFrom) {
            Node restriction = new Node();
            restriction.setId(UUID.randomUUID().toString());
            restriction.setUri(null);
            restriction.setLabel(OwlConstants.LABEL_RESTRICTION);

            Property onPropertyP = new Property();
            onPropertyP.setKey(OwlConstants.PROPERTY_KEY_ON_PROPERTY);
            onPropertyP.setValue(svf.getProperty().getNamedProperty().getIRI().toURI().toASCIIString());

            Property someValuesFromP = new Property();
            someValuesFromP.setKey(OwlConstants.PROPERTY_KEY_SOME_VALUES_FROM);
            someValuesFromP.setValue(svf.getFiller().asOWLClass().getIRI().toURI().toASCIIString());
            restriction.getProperties().add(onPropertyP);
            restriction.getProperties().add(someValuesFromP);


            Relation relationSubClassOf = new Relation();
            relationSubClassOf.setLabel(OwlConstants.LABEL_SUBCLASS_OF);
            relationSubClassOf.setId(UUID.randomUUID().toString());
            relationSubClassOf.setFrom(GraphUtils.findAlreadyCreatedClass(clazz.getIRI(), graph));
            relationSubClassOf.setTo(restriction);

            graph.addNode(restriction);
            graph.getRelations().add(relationSubClassOf);
        }

        return graph;
    }
}
