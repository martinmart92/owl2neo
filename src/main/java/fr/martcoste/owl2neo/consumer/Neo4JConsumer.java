package fr.martcoste.owl2neo.consumer;

import com.google.inject.Inject;
import fr.martcoste.owl2neo.conf.arguments.Constants;
import fr.martcoste.owl2neo.conf.arguments.JavaPropertiesArguments;
import fr.martcoste.owl2neo.model.graph.GraphMetadata;
import fr.martcoste.owl2neo.model.graph.Node;
import fr.martcoste.owl2neo.model.graph.Relation;
import fr.martcoste.owl2neo.neo.PropertiesUtil;
import fr.martcoste.owl2neo.neo.QueryReport;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.exceptions.ClientException;
import org.neo4j.driver.v1.summary.ResultSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.StringJoiner;

/**
 * Created by martin on 20/08/16.
 */
public class Neo4JConsumer implements ConsumerIfc {

    private String neoURL;
    private String username;
    private String password;

    private Driver driver;
    private final boolean cleanOnStart;

    private QueryReport queryReport = new QueryReport();

    protected static final Logger LOG = LoggerFactory.getLogger(Neo4JConsumer.class);

    @Inject
    public Neo4JConsumer(@Nonnull JavaPropertiesArguments args) {
        neoURL = args.getArguments().get(Constants.NEO_REPOSITORY_URL);
        username = args.getArguments().get(Constants.NEO_REPOSITORY_LOGIN);
        password = args.getArguments().get(Constants.NEO_REPOSITORY_PASSWORD);
        cleanOnStart = Boolean.parseBoolean(args.getArguments().get(Constants.NEO_CLEAN_ON_START));
    }

    @Override
    public void init() throws ConsumerException {
        LOG.debug(String.format("init neo4j driver from url: '%s', with username : '%s', with password '%s'",
                this.neoURL, this.username, this.password));
        this.driver = GraphDatabase.driver(neoURL,
                AuthTokens.basic(username, password));

        if (cleanOnStart) {
            cleanOnStart();
        }
    }

    private void cleanOnStart() throws ConsumerException {
        LOG.debug("cleanOnStart");
        String cypher = "MATCH (n) DETACH DELETE n";
        run(cypher);
        printReport();
    }

    @Override
    public void consume(@Nonnull GraphMetadata graphMeta) throws ConsumerException {
        LOG.debug("consume GraphMetadata");

        String template = "CREATE (onto:Ontology {%s})";

        PropertiesUtil util = new PropertiesUtil();
        String cp = util.toCypherProperties(graphMeta.getProperties());

        run(String.format(template, cp));
    }


    @Override
    public void consume(@Nonnull Node node) throws ConsumerException {
        LOG.debug("consume Node");

        PropertiesUtil util = new PropertiesUtil();
        String cp = util.toCypherProperties(node.getProperties());

        StringJoiner sj = new StringJoiner(", ", "{", "}");
        sj.add("id:\"%s\"");
        sj.add("uri:\"%s\"");

        if (!cp.isEmpty()) {
            sj.add("%s");
        }

        String template = "CREATE (n:%s "+sj.toString()+")";

        run(String.format(template, node.getLabel(), node.getId(), node.getUri(), cp));
    }

    @Override
    public void consume(@Nonnull Relation relation) throws ConsumerException {
        LOG.debug("consume Relation");

        PropertiesUtil util = new PropertiesUtil();
        String cp = util.toCypherProperties(relation.getProperties());

        StringJoiner sj = new StringJoiner(", ", "{", "}");
        sj.add("id:\"%s\"");

        if (!cp.isEmpty()) {
            sj.add("%s");
        }

        String template1 = "MATCH  (from {id:\""+relation.getFrom().getId()+"\"})\n";
        String template2 = "MATCH  (to {id:\""+relation.getTo().getId()+"\"})\n";
        String template3 = "CREATE (from)-[:%s "+sj.toString()+"]->(to)";
        String template = template1 + template2 + template3;

        run(String.format(template, relation.getLabel(), relation.getId(), cp));
    }


    @Override
    public void finish() {
        LOG.debug("finish");
        driver.session().close();
        driver.close();
        printReport();

    }


    /**
     * @param cypherQuery string in Cypher language
     *                    example: "CREATE (a:Person {name:'Arthur', title:'King'})"
     * @return
     */
    protected
    @Nonnull
    ResultSummary run(@Nonnull String cypherQuery) throws ConsumerException {
        LOG.debug("run cypher query: " + cypherQuery);
        Session session = driver.session();
        try {
            long startTime = System.currentTimeMillis();
            ResultSummary result = session.run(cypherQuery).consume();
            long endTime = System.currentTimeMillis();
            addToReport(cypherQuery, result, (endTime - startTime));
            return result;
        } catch (ClientException e) {
            throw new ConsumerException("an error occurred while running cypher statement", e);
        } finally {
            session.close();
        }
    }

    private void addToReport(@Nonnull String cypherQuery, @Nonnull ResultSummary result, long timeInMillis) {
        queryReport.addToReport(cypherQuery, result, new Date(), timeInMillis);
    }

    private void printReport() {
        queryReport.printReport();
    }
}
