package fr.martcoste.owl2neo.consumer;

import fr.martcoste.owl2neo.model.graph.GraphMetadata;
import fr.martcoste.owl2neo.model.graph.Node;
import fr.martcoste.owl2neo.model.graph.Relation;

import javax.annotation.Nonnull;

/**
 * Created by martin on 20/08/16.
 */
public interface ConsumerIfc {

    /**
     * init
     */
    void init() throws ConsumerException;

    /**
     * consume graph metadata
     * @param graphMeta
     */
    void consume(@Nonnull GraphMetadata graphMeta) throws ConsumerException;


    /**
     * consume node
     * @param node
     */
    void consume(@Nonnull Node node) throws ConsumerException;

    /**
     * consume relation
     * @param relation
     */
    void consume(@Nonnull Relation relation) throws ConsumerException;

    /**
     *
     */
    void finish();
}
