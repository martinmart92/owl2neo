package fr.martcoste.owl2neo.consumer;

import fr.martcoste.owl2neo.model.graph.GraphMetadata;
import fr.martcoste.owl2neo.model.graph.Node;
import fr.martcoste.owl2neo.model.graph.Relation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

/**
 * Created by martin on 20/08/16.
 */
public class CountConsumer implements ConsumerIfc {

    protected static final Logger LOG = LoggerFactory.getLogger(CountConsumer.class);

    private long meta = 0;
    private int nodes = 0;
    private int relations = 0;

    @Override
    public void init() {

    }

    @Override
    public void consume(@Nonnull GraphMetadata graphMeta) {
        meta = graphMeta.getProperties().stream().count();
    }

    @Override
    public void consume(@Nonnull Node node) {
        this.nodes++;
    }

    @Override
    public void consume(@Nonnull Relation relation) {
        relations++;
    }


    @Override
    public void finish() {
        LOG.info("Count Consumer finish()");
        LOG.info("Count Meta "+ meta);
        LOG.info("Count Nodes "+ nodes);
        LOG.info("Count Relations "+ relations);
    }
}
