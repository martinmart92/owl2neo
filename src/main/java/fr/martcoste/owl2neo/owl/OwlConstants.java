package fr.martcoste.owl2neo.owl;

import org.semarglproject.vocab.OWL;

/**
 * Created by martin on 21/08/16.
 */
public final class OwlConstants {

    /*
    label in sense of neo4j label (aka type)
     */
    public static final String LABEL_OWL_CLASS = "OwlClass";
    public static final String LABEL_RESTRICTION = "Restriction";
    public static final String PROPERTY_KEY_ON_PROPERTY = OWL.ON_PROPERTY;
    public static final String PROPERTY_KEY_SOME_VALUES_FROM = OWL.SOME_VALUES_FROM;
    public static final String LABEL_SUBCLASS_OF = "SubClassOf";
    public static final String LABEL_DISJOINT_WITH = "DisjointWith";
    public static final String PROPERTY_KEY_ALL_VALUES_FROM = OWL.ALL_VALUES_FROM;
}
