package fr.martcoste.owl2neo.neo;

import fr.martcoste.owl2neo.model.graph.Property;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * Created by martin on 21/08/16.
 */
public class PropertiesUtil {

    public static final String PROPERTY_VALUES_SEPARATOR = "//";

    @Nonnull
    public String toCypherProperties(@Nonnull Set<Property> properties) {
        StringJoiner sj = new StringJoiner(",");
        Map<String, Set<Property>> groupedByKey = groupPropertiesByKey(properties);

        groupedByKey.keySet()
                .forEach(k -> {
                    StringBuilder sb = new StringBuilder();
                    sb.append("`" + k + "`").append(":");
                    StringJoiner joiner = new StringJoiner(",", "[", "]");
                    groupedByKey.get(k).forEach(v -> {
                        joiner.add("\"" + propertyToJson(v) + "\"");
                    });
                    sb.append(joiner.toString());
                    sj.add(sb.toString());
                });
        return sj.toString();
    }

    protected String propertyToJson(@Nonnull Property p) {
        return p.getValue() + PROPERTY_VALUES_SEPARATOR + p.getLanguage() + PROPERTY_VALUES_SEPARATOR + p.getDatatype();
    }

    protected Map<String, Set<Property>> groupPropertiesByKey(@Nonnull Set<Property> properties) {
        Map<String, Set<Property>> map = new HashMap<>();
        properties.forEach(
                p -> {
                    Set<Property> set = null;
                    if (map.containsKey(p.getKey())) {
                        set = map.get(p.getKey());
                    } else {
                        set = new HashSet<>();
                    }
                    set.add(p);
                    map.put(p.getKey(), set);
                }
        );
        return map;
    }
}
