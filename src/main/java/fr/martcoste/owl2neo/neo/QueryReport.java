package fr.martcoste.owl2neo.neo;

import org.neo4j.driver.v1.summary.ResultSummary;
import org.neo4j.driver.v1.summary.SummaryCounters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.OptionalDouble;

/**
 * Created by martin on 21/08/16.
 */
public class QueryReport {
    protected static final Logger LOG = LoggerFactory.getLogger(QueryReport.class);

    private DateFormat formatter = SimpleDateFormat.getDateTimeInstance();

    private List<QueryExecution> report = new ArrayList<>();

    private boolean detailedByQuery = false;

    public void printReport() {
        LOG.debug("report:");
        printIndividualQueries();
        printGlobalCounters();

    }

    private void printIndividualQueries() {
        report.forEach(q ->
                LOG.debug("run query: '"+q.getCypherQuery()+"'" +
                        " at " + formatDate(q.getDate())
                        + (detailedByQuery?("\n" + formatStatementResult(q.getResultSummary())):"")
                )
        );
    }
    private void printGlobalCounters() {

        Integer nodesCreated = report.stream()
                .map(QueryExecution::getResultSummary)
                .map(ResultSummary::counters)
                .mapToInt(SummaryCounters::nodesCreated)
                .sum();
        Integer relationshipsCreated = report.stream()
                .map(QueryExecution::getResultSummary)
                .map(ResultSummary::counters)
                .mapToInt(SummaryCounters::relationshipsCreated)
                .sum();
        Integer indexesAdded = report.stream()
                .map(QueryExecution::getResultSummary)
                .map(ResultSummary::counters)
                .mapToInt(SummaryCounters::indexesAdded)
                .sum();
        Integer constraintsAdded = report.stream()
                .map(QueryExecution::getResultSummary)
                .map(ResultSummary::counters)
                .mapToInt(SummaryCounters::constraintsAdded)
                .sum();
        Integer labelsAdded = report.stream()
                .map(QueryExecution::getResultSummary)
                .map(ResultSummary::counters)
                .mapToInt(SummaryCounters::labelsAdded)
                .sum();
        Integer nodesDeleted = report.stream()
                .map(QueryExecution::getResultSummary)
                .map(ResultSummary::counters)
                .mapToInt(SummaryCounters::nodesDeleted)
                .sum();
        Integer relationshipsDeleted = report.stream()
                .map(QueryExecution::getResultSummary)
                .map(ResultSummary::counters)
                .mapToInt(SummaryCounters::relationshipsDeleted)
                .sum();
        Integer indexesRemoved = report.stream()
                .map(QueryExecution::getResultSummary)
                .map(ResultSummary::counters)
                .mapToInt(SummaryCounters::indexesRemoved)
                .sum();
        Integer constraintsRemoved = report.stream()
                .map(QueryExecution::getResultSummary)
                .map(ResultSummary::counters)
                .mapToInt(SummaryCounters::constraintsRemoved)
                .sum();

        Integer labelsRemoved = report.stream()
                .map(QueryExecution::getResultSummary)
                .map(ResultSummary::counters)
                .mapToInt(SummaryCounters::labelsRemoved)
                .sum();

        Integer containsUpdates = report.stream()
                .map(QueryExecution::getResultSummary)
                .map(ResultSummary::counters)
                .mapToInt(update -> {
                    return update.containsUpdates()?1:0;
                })
                .sum();

        OptionalDouble averageQueryTime = report.stream()
                .map(QueryExecution::getTimeInMillis)
                .mapToLong(t -> {
                    return t;
                })
                .average();

        LOG.debug("GLOBAL Counter summary:");
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append("created nodes: " + nodesCreated+"\n");
        sb.append("created relationships: " + relationshipsCreated+"\n");
        sb.append("added indexes: " + indexesAdded+"\n");
        sb.append("added indexes: " + indexesAdded+"\n");
        sb.append("added constraints: " + constraintsAdded+"\n");
        sb.append("added labels: " + labelsAdded+"\n");
        sb.append("deleted nodes: " + nodesDeleted+"\n");
        sb.append("deleted relationships: " + relationshipsDeleted+"\n");
        sb.append("removed indexes: " + indexesRemoved+"\n");
        sb.append("removed constraints: " + constraintsRemoved+"\n");
        sb.append("removed labels: " + labelsRemoved+"\n");
        sb.append("contains updates: "+containsUpdates+"\n");
        sb.append("avg time(ms)/query: "+(averageQueryTime.isPresent()?averageQueryTime.getAsDouble():"unknown"));
        LOG.debug(sb.toString());
    }


    public void addToReport(String cypherQuery, ResultSummary result, Date d, long timeInMillis) {
        QueryExecution queryExec = new QueryExecution();
        queryExec.setCypherQuery(cypherQuery);
        queryExec.setDate(d);
        queryExec.setResultSummary(result);
        queryExec.setTimeInMillis(timeInMillis);
        report.add(queryExec);
    }

    private String formatStatementResult(@Nonnull ResultSummary result) {
        StringBuilder sb = new StringBuilder();
        SummaryCounters counters = result.counters();
        if (counters.nodesCreated() > 0) {
            sb.append("created nodes: " + counters.nodesCreated());
            sb.append("\n");
        }
        if (counters.relationshipsCreated() > 0) {
            sb.append("created relationships: " + counters.relationshipsCreated());
            sb.append("\n");
        }
        if (counters.indexesAdded() > 0) {
            sb.append("added indexes: " + counters.indexesAdded());
            sb.append("\n");
        }
        if (counters.constraintsAdded() > 0) {
            sb.append("added constraints: " + counters.constraintsAdded());
            sb.append("\n");
        }
        if (counters.labelsAdded() > 0) {
            sb.append("added labels: " + counters.labelsAdded());
            sb.append("\n");
        }
        if (counters.nodesDeleted() > 0) {
            sb.append("deleted nodes: " + counters.nodesDeleted());
            sb.append("\n");
        }
        if (counters.relationshipsDeleted() > 0) {
            sb.append("deleted relationships: " + counters.relationshipsDeleted());
            sb.append("\n");
        }
        if (counters.indexesRemoved() > 0) {
            sb.append("removed indexes: " + counters.indexesRemoved());
            sb.append("\n");
        }
        if (counters.constraintsRemoved() > 0) {
            sb.append("removed constraints: " + counters.constraintsRemoved());
            sb.append("\n");
        }
        if (counters.labelsRemoved() > 0) {
            sb.append("removed labels: " + counters.labelsRemoved());
            sb.append("\n");
        }
        if (counters.containsUpdates()) {
            sb.append("contains updates.");
        }
        return sb.toString();
    }

    private String formatDate(@Nonnull Date d) {
        return formatter.format(d);
    }
}
