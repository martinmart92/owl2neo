package fr.martcoste.owl2neo.neo;

import org.neo4j.driver.v1.summary.ResultSummary;

import java.util.Date;

/**
 * Created by martin on 21/08/16.
 */
public class QueryExecution {


    private String cypherQuery;
    private Date date;
    private ResultSummary resultSummary;
    private long timeInMillis;

    public void setCypherQuery(String cypherQuery) {
        this.cypherQuery = cypherQuery;
    }

    public String getCypherQuery() {
        return cypherQuery;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setResultSummary(ResultSummary resultSummary) {
        this.resultSummary = resultSummary;
    }

    public ResultSummary getResultSummary() {
        return resultSummary;
    }

    public void setTimeInMillis(long timeInMillis) {
        this.timeInMillis = timeInMillis;
    }

    public long getTimeInMillis() {
        return timeInMillis;
    }
}
