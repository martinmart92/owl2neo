package fr.martcoste.owl2neo.read;

import fr.martcoste.owl2neo.model.owl.OntologyIfc;

import javax.annotation.Nonnull;

/**
 * Created by martin on 20/08/16.
 */
public interface OwlReaderIfc {

    @Nonnull
    OntologyIfc readFile() throws ReaderException;
}
