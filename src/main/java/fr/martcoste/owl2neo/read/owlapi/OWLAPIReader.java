package fr.martcoste.owl2neo.read.owlapi;

import com.google.inject.Inject;
import fr.martcoste.owl2neo.conf.arguments.Constants;
import fr.martcoste.owl2neo.conf.arguments.JavaPropertiesArguments;
import fr.martcoste.owl2neo.model.owl.OntologyIfc;
import fr.martcoste.owl2neo.model.owl.owlapi.OWLApiOntology;
import fr.martcoste.owl2neo.read.OwlReaderIfc;
import fr.martcoste.owl2neo.read.ReaderException;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;

/**
 * Created by martin on 20/08/16.
 */
public class OWLAPIReader implements OwlReaderIfc {

    protected static final Logger LOG = LoggerFactory.getLogger(OWLAPIReader.class);

    private final String inputFile;

    @Inject
    public OWLAPIReader(@Nonnull JavaPropertiesArguments args) {
        inputFile = args.getArguments().get(Constants.OWL_INPUT_FILE);
    }

    @Override
    public OntologyIfc readFile() throws ReaderException {

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        try {
            OWLOntology ontology = load(manager);
            OWLApiOntology output = new OWLApiOntology();
            output.setOntology(ontology);
            return output;

        } catch (OWLOntologyCreationException e) {
            throw new ReaderException("error at loading the ontology from file '"
                    + this.inputFile + "'", e);
        }
    }


    @Nonnull
    OWLOntology load(@Nonnull OWLOntologyManager manager) throws OWLOntologyCreationException {
        // in this test, the ontology is loaded from a string
        LOG.debug("loading ontology from file:'" + inputFile + "'");
        return manager.loadOntologyFromOntologyDocument(new File(inputFile));
    }
}
