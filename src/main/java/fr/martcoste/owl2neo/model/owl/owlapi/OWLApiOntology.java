package fr.martcoste.owl2neo.model.owl.owlapi;

import fr.martcoste.owl2neo.model.owl.OntologyIfc;
import org.semanticweb.owlapi.model.OWLOntology;

/**
 * Created by martin on 20/08/16.
 */
public class OWLApiOntology implements OntologyIfc {

    private OWLOntology ontology;

    public OWLOntology getOntology() {
        return ontology;
    }

    public void setOntology(OWLOntology ontology) {
        this.ontology = ontology;
    }
}
