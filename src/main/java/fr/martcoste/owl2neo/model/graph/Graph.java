package fr.martcoste.owl2neo.model.graph;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by martin on 20/08/16.
 */
public class Graph {

    private GraphMetadata metadata;
    private Set<Node> nodes = new HashSet<>();
    private Set<Relation> relations = new HashSet<>();

    private GraphCache cache = new GraphCache();

    public GraphMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(GraphMetadata metadata) {
        this.metadata = metadata;
    }

    public void addNode(@Nonnull Node n) {
        this.getNodes().add(n);
        this.cache.addNode(n);
    }

    public void addAllNodes(@Nonnull List<Node> nodes) {
        nodes.forEach(this::addNode);
    }
    public Node getNode(@Nonnull String uri) {
        return this.cache.getNode(uri);
    }
    private Set<Node> getNodes() {
        return nodes;
    }

    private void setNodes(Set<Node> nodes) {
        this.nodes = nodes;
    }

    public Set<Relation> getRelations() {
        return relations;
    }

    public void setRelations(Set<Relation> relations) {
        this.relations = relations;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Graph graph = (Graph) o;

        if (metadata != null ? !metadata.equals(graph.metadata) : graph.metadata != null) return false;
        if (nodes != null ? !nodes.equals(graph.nodes) : graph.nodes != null) return false;
        return relations != null ? relations.equals(graph.relations) : graph.relations == null;

    }

    @Override
    public int hashCode() {
        int result = metadata != null ? metadata.hashCode() : 0;
        result = 31 * result + (nodes != null ? nodes.hashCode() : 0);
        result = 31 * result + (relations != null ? relations.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Graph{" +
                "metadata=" + metadata +
                '}';
    }

    public Stream<Node> getNodesParallelStream() {
        return this.getNodes().parallelStream();
    }
    public Stream<Node> getNodesStream() {
        return this.getNodes().stream();
    }
}
