package fr.martcoste.owl2neo.model.graph;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by martin on 20/08/16.
 */
public abstract class Resource {


    private String label;//aka main "type" e.g.:Person, OWLClass, Restriction, etc.
    private String id;
    private Set<Property> properties = new HashSet<>();


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Set<Property> getProperties() {
        return properties;
    }

    public void setProperties(Set<Property> properties) {
        this.properties = properties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Resource resource = (Resource) o;

        if (label != null ? !label.equals(resource.label) : resource.label != null) return false;
        if (id != null ? !id.equals(resource.id) : resource.id != null) return false;
        return properties != null ? properties.equals(resource.properties) : resource.properties == null;

    }

    @Override
    public int hashCode() {
        int result = label != null ? label.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (properties != null ? properties.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Resource{" +
                "label='" + label + '\'' +
                ", id='" + id + '\'' +
                ", properties=" + properties +
                '}';
    }
}
