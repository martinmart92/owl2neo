package fr.martcoste.owl2neo.model.graph;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by martin on 21/08/16.
 */
public class GraphCache {

    private Map<String, Node> nodeByURI = new HashMap<>();

    public void addNode(@Nonnull Node node) {
        this.nodeByURI.put(node.getUri(), node);
    }
    public Node getNode(@Nonnull String uri) {
        return this.nodeByURI.get(uri);
    }
}
