package fr.martcoste.owl2neo.model.graph;

/**
 * Created by martin on 20/08/16.
 */
public class Property {

    private String key;//aka "type". e.g:birthDate
    private String value;
    private String datatype;
    private String language;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Property property = (Property) o;

        if (key != null ? !key.equals(property.key) : property.key != null) return false;
        if (value != null ? !value.equals(property.value) : property.value != null) return false;
        if (datatype != null ? !datatype.equals(property.datatype) : property.datatype != null) return false;
        return language != null ? language.equals(property.language) : property.language == null;

    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (datatype != null ? datatype.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Property{" +
                "key='" + key + '\'' +
                ", value='" + value + '\'' +
                ", datatype='" + datatype + '\'' +
                ", language='" + language + '\'' +
                '}';
    }
}
