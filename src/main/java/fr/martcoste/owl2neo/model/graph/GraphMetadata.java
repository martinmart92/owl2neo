package fr.martcoste.owl2neo.model.graph;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by martin on 20/08/16.
 */
public class GraphMetadata {

    private Set<Property> properties = new HashSet<>();


    public Set<Property> getProperties() {
        return properties;
    }

    public void setProperties(Set<Property> properties) {
        this.properties = properties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GraphMetadata that = (GraphMetadata) o;

        return properties != null ? properties.equals(that.properties) : that.properties == null;

    }

    @Override
    public int hashCode() {
        return properties != null ? properties.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "GraphMetadata{" +
                "properties=" + properties +
                '}';
    }
}
