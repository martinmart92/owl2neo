package fr.martcoste.owl2neo.model.graph;

/**
 * Created by martin on 20/08/16.
 */
public class Node extends Resource {

    private String uri;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Node node = (Node) o;

        return uri != null ? uri.equals(node.uri) : node.uri == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (uri != null ? uri.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Node{" +
                "uri='" + uri + '\'' +
                "} " + super.toString();
    }
}
