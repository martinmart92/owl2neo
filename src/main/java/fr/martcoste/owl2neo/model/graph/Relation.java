package fr.martcoste.owl2neo.model.graph;

/**
 * Created by martin on 20/08/16.
 */
public class Relation extends Resource {

    private Node from;
    private Node to;

    public Node getFrom() {
        return from;
    }

    public void setFrom(Node from) {
        this.from = from;
    }

    public Node getTo() {
        return to;
    }

    public void setTo(Node to) {
        this.to = to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Relation relation = (Relation) o;

        if (from != null ? !from.equals(relation.from) : relation.from != null) return false;
        return to != null ? to.equals(relation.to) : relation.to == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (from != null ? from.hashCode() : 0);
        result = 31 * result + (to != null ? to.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Relation{" +
                "from=" + from +
                ", to=" + to +
                "} " + super.toString();
    }
}
