package fr.martcoste.owl2neo.utils;

import fr.martcoste.owl2neo.model.graph.Graph;
import fr.martcoste.owl2neo.model.graph.Node;
import org.semanticweb.owlapi.model.IRI;

import javax.annotation.Nonnull;

/**
 * Created by martin on 23/08/16.
 */
public class GraphUtils {


    @Nonnull
    public static Node findAlreadyCreatedClass(@Nonnull IRI iri, @Nonnull Graph graph) {
        return graph.getNode(iri.toURI().toASCIIString());
    }
}
